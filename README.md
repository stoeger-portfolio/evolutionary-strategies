# Evolutionstratagy

A simple projet where the equasion 3x²+5y³ = 7a+3b² gets solved with the help of evolutionary strategies.

## Used Strategies
The strategies were once executed 100 times with and allowed acceptance of 0.000002f, 0.00002f, 0.02f and 0.2f. The results are shown below. <br/>
In the following diagrams the x-axis is the number of generations and the y-axis is the difference between the left and right part of the equasion. The diagrams were created by a single execution of the algorithm with an acceptance rate of 0.000002;
![](https://lh6.googleusercontent.com/NHvKWcyCklNCP3f5HGBzd_w4YNQuUF3hMNSZAkWxGvch8eJz6Inf2aBqzFXxmthgS7ZjfUvZRfURzt4wiIeepZJaGnROxbAqRzh0putlECcyNSxJrmLHq8Lb72bD89TNsKBclJZn "Points scored")
Average Generations: 4.012.264,25 (acceptance 0.000002f) <br/>
Average Generations: 438.247,19 (acceptance 0.0002f) <br/>
Average Generations: 389,47 (acceptance 0.02f) <br/>
Average Generations: 55,61 (acceptance 0.2f) <br/>

![](https://lh4.googleusercontent.com/bxMr3VdikujPAcnV4ohEMyGg6hDZ1xVYy1xYB0w10Vhbq--jxPFchlvaSQ0Zeyx0LXm_JADJNqKs3zHBp6ebKNHsFrz-CXya7ORxZJfVJLxvn-F3mXwz3sgXnkd-rx_95iZbYX8u "Points scored")
Average Generations: 2498,57 (acceptance 0.000002f) <br/>
Average Generations: 243,42 (acceptance 0.0002f) <br/>
Average Generations: 2,75 (acceptance 0.02f) <br/>
Average Generations: 2,02 (acceptance 0.2f) <br/>
![](https://lh4.googleusercontent.com/pN7XdsJDYbKS2ljXMNEApEB7wPpmc1VJiyrykN5AYDnI2GPQ4dI5d-jJNY1PrUZn_OYfWG7CVFq7hX24eXUWTirHc1LZP2szV4T9AqMPRRK9x0xXZrzUxCVcXEUGQV8FiuzqOMOP "Points scored")
Average Generations: 1085,74 (acceptance 0.000002f) <br/>
Average Generations: 940,20 (acceptance 0.0002f) <br/>
Average Generations: 2,12 (acceptance 0.02f) <br/>
Average Generations: 2,00 (acceptance 0.2f) <br/>
![](https://lh6.googleusercontent.com/tfp34qiWVy7W2-tiauLyaMTqkbFgnBfzaeaBf1QfhF5sNL36E3hapW_Ga8pT425007gkFE0BF-fiQHqXmz0kfG9s0U-2-cv7E0btX33jpWiTX6FNHmOlQgiOyKQHWmOu7hIaB0ZN "Points scored")
Establishing the genomes by fusion e.g: <1, 2, 3, 4>, <5, 6, 7, 8> -> <3,4,5,6> <br/>
Average Generations: 4415,39 (acceptance 0.000002f) <br/>
Average Generations: 586,51 (acceptance 0.0002f) <br/>
Average Generations: 2,10 (acceptance 0.02f) <br/>
Average Generations: 2,00 (acceptance 0.2f) <br/>
![](https://lh5.googleusercontent.com/ZLMQeb2UXqFvoAF2cSXUL5mKZfOlPXWD65qI0QVyesKVNcQsNoaPEsx66Nkq2_tBiWvFrKzOjEpzGU-r-tTZD2WtDRHBx08K6pYiUUaZX0rR7ZUaely4gLsdIPL9Kmu_fvfwf9x3 "Points scored")
Establishing the genomes by fusion e.g: <1, 2, 3, 4>, <5, 6, 7, 8> -> <3,4,5,6> <br/>
Average Generations: 4460,59 (acceptance 0.000002f) <br/>
Average Generations: 530,95 (acceptance 0.0002f) <br/>
Average Generations: 2,15 (acceptance 0.02f) <br/>
Average Generations: 2,00 (acceptance 0.2f) <br/>
