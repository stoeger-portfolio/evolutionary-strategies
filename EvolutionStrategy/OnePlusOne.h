#pragma once
#include "Helper.h"
#include <random>

class OnePlusOne
{
public:
	OnePlusOne(float noise, float acceptance, float minRnd, float maxRnd);
	~OnePlusOne();
	int Start();

private:
	std::random_device m_random_device;
	std::mt19937_64 m_generator;
	std::uniform_real_distribution<float> m_distribution;

	float m_noise;
	float m_acceptance;

	void Mutate();
	void Select(int generation);

	Helper::genom m_parent;
	Helper::genom m_child;
};

