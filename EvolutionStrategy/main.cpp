#include <stdio.h>
#include <Windows.h>
#include "OnePlusOne.h"
#include "MyPlusLambda.h"
#include "MyCommaLambda.h"
#include "MySlashRhoPlusLambda.h"
#include "MySlashRhoCommaLambda.h"

int main(void)
{
	float noise = 0.1f;
	float acceptance = 0.000002f;
	float minRnd = -2.0f;
	float maxRnd = 3.0f;
	int numberParents = 100;
	int numberChildren = 500;
	int numberDirectParents = 5;

	OnePlusOne *OP = new OnePlusOne(noise, acceptance, minRnd, maxRnd);
	MyPlusLambda *MPL = new MyPlusLambda(noise, acceptance, minRnd, maxRnd, numberParents, numberChildren);
	MyCommaLambda *MCL = new MyCommaLambda(noise, acceptance, minRnd, maxRnd, numberParents, numberChildren);
	MySlashRhoPlusLambda *MSRPL = new MySlashRhoPlusLambda(noise, acceptance, minRnd, maxRnd, numberParents, numberChildren, numberDirectParents);
	MySlashRhoCommaLambda *MSRCL = new MySlashRhoCommaLambda(noise, acceptance, minRnd, maxRnd, numberParents, numberChildren, numberDirectParents);
	
	float OPg = 0.0f;
	float MPLg = 0.0f;
	float MCLg = 0.0f;
	float MSRPLg = 0.0f;
	float MSRCLg = 0.0f;

	int numberOfTries = 100;

	for (int i = 0; i < numberOfTries; ++i)
	{
		OPg += OP->Start();
	}

	for (int i = 0; i < numberOfTries; ++i)
	{
		MPLg += MPL->Start();
	}
	
	for (int i = 0; i < numberOfTries; ++i)
	{
		MCLg += MCL->Start();
	}
	
	for (int i = 0; i < numberOfTries; ++i)
	{
		MSRPLg += MSRPL->Start();
	}
	
	for (int i = 0; i < numberOfTries; ++i)
	{
		MSRCLg += MSRCL->Start();
	}

	printf("(1+1) Average Generations: %f\n", (float)OPg / (float)numberOfTries);
	printf("(%c+Lambda) Average Generations: %f\n", 230, (float)MPLg   / (float)numberOfTries);
	printf("(%c,Lambda) Average Generations: %f\n", 230, (float)MCLg   / (float)numberOfTries);
	printf("(%c/rho+Lambda) Average Generations: %f\n", 230, (float)MSRPLg / (float)numberOfTries);
	printf("(%c/rho,Lambda) Average Generations: %f\n", 230, (float)MSRCLg / (float)numberOfTries);

	delete OP;
	delete MPL;
	delete MCL;
	delete MSRPL;
	delete MSRCL;
}