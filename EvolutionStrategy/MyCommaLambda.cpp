#include "MyCommaLambda.h"
#include <random>
#include <assert.h>
#include <algorithm>
#include <iostream>
#include <cstdlib>
#include <climits>
#include <iostream>

MyCommaLambda::MyCommaLambda(float noise, float acceptance, float minRnd, float maxRnd, uint16_t numberParents, uint16_t numberChildren) :
	m_noise(noise),
	m_numberParents(numberParents),
	m_numberChildren(numberChildren),
	m_acceptance(acceptance),
	m_generator(std::mt19937(m_random_device())),
	m_distribution(std::uniform_real_distribution<float>(minRnd, maxRnd)),
	m_bestPopulation(new Helper::genom()),
	m_children(new Helper::genom[numberChildren]),
	m_parents(new Helper::genom[numberParents]),
	m_intrnd(std::uniform_int_distribution<int>(0, m_numberParents))
{
	assert(numberChildren > 0.0f && "PopulationNumber needs to be greater than zero!");
	printf("Constructor (%c,Lambda)\n", 230);
}

MyCommaLambda::~MyCommaLambda()
{
	delete[] m_parents;
	delete[] m_children;
	delete m_bestPopulation;
}

int MyCommaLambda::Start()
{
	printf("Start (%c,Lambda)\n", 230);
	this->GenerateParents();

	std::qsort(m_parents, m_numberParents, sizeof(Helper::genom), Helper::Sort);
	printf("New Difference: %f\n After: 0 Generations\n\n", m_parents[0].result);

	int generations = 1;
	while (!(m_bestPopulation->result < m_acceptance))
	{
		this->Mutate();
		this->Select(generations);
		generations++;
	}

	printf("Solution found after %u generations!\n\n", generations);
	printf("Accepted a difference of max: %f\n", m_acceptance);

	printf("Found x: %1.8f\n", m_bestPopulation->x);
	printf("Found y: %1.8f\n", m_bestPopulation->y);
	printf("Found a: %1.8f\n", m_bestPopulation->a);
	printf("Found b: %1.8f\n\n", m_bestPopulation->b);

	if (m_bestPopulation->a > m_bestPopulation->b)
	{
		printf("a > b = true\n\n");
	}

	printf("Left Term result: %1.8f\n", (powl(m_bestPopulation->x, 2.0f) * 3) + (powl(m_bestPopulation->y, 3.0f) * 5));
	printf("Right Term result: %1.8f\n", (7 * m_bestPopulation->a) + (powl(m_bestPopulation->b, 2.0f) * 3));
	printf("Actual difference: %f\n\n", m_bestPopulation->result);

	return generations;
}

void MyCommaLambda::GenerateParents()
{
	float tmpa;
	float tmpb;

	m_bestPopulation->result = std::numeric_limits<float>::max();
	for (int i = 0; i < this->m_numberParents; ++i)
	{
		m_parents[i].x = m_distribution(m_generator);
		m_parents[i].y = m_distribution(m_generator);
		tmpa = m_distribution(m_generator);
		tmpb = m_distribution(m_generator);

		if (tmpa > tmpb)
		{
			m_parents[i].a = tmpa;
			m_parents[i].b = tmpb;
		}
		else {
			m_parents[i].a = tmpb;
			m_parents[i].b = tmpa;
		}
		m_parents[i].result = Helper::CalculatePositiveDifference(m_parents[i]);
	}
}

void MyCommaLambda::Select(int generation)
{
	// Sort array elements by distance
	std::qsort(m_children, m_numberChildren, sizeof(Helper::genom), Helper::Sort);

	if (m_bestPopulation->result > m_children[0].result) {
		memcpy(&m_bestPopulation[0], &m_children[0], sizeof(Helper::genom));
		// printf("New Difference: %f\n After: %u Generations\n\n", m_bestPopulation->result, generation);
	}
	memcpy(&m_parents[0], &m_children[0], m_numberParents * sizeof(Helper::genom));
}

void MyCommaLambda::Mutate()
{
	float tmpa;
	float tmpb;
	int parentIndex;
	for (uint32_t i = 0; i < m_numberChildren; ++i)
	{
		parentIndex = m_intrnd(m_generator);

		tmpa = m_distribution(m_generator);
		tmpb = m_distribution(m_generator);

		if (tmpa > tmpb)
		{
			m_children[i].a = m_parents[parentIndex].a + m_noise * tmpa;
			m_children[i].b = m_parents[parentIndex].b + m_noise * tmpb;
		}
		else {
			m_children[i].a = m_parents[parentIndex].a + m_noise * tmpb;
			m_children[i].b = m_parents[parentIndex].b + m_noise * tmpa;
		}

		m_children[i].x = m_parents[parentIndex].x + m_noise * m_distribution(m_generator);
		m_children[i].y = m_parents[parentIndex].y + m_noise * m_distribution(m_generator);
		m_children[i].result = Helper::CalculatePositiveDifference(m_children[i]);
	}
}