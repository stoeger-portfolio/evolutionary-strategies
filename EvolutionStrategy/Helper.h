#pragma once
#include <math.h>
namespace Helper
{
	struct genom {
		float x;
		float y;
		float a;
		float b;
		float result;
	};


	inline static float CalculateLeftTerm(const genom &g)
	{
		return (powf(g.x, 2.0f) * 3) + (powf(g.y, 3.0f) * 5);
	}

	inline static float CalculateRightTerm(const genom &g)
	{
		return (7 * g.a) + (powf(g.b, 2.0f) * 3);
	}

	inline static float CalculatePositiveDifference(const genom &g)
	{
		float difference = CalculateLeftTerm(g) - CalculateRightTerm(g);

		return difference < 0 ? -difference : difference;
	}

	inline static int Sort(const void* a, const void* b) {
		const Helper::genom arg1 = *static_cast<const Helper::genom*>(a);
		const Helper::genom arg2 = *static_cast<const Helper::genom*>(b);

		if (arg1.result < arg2.result) return -1;
		if (arg2.result < arg1.result) return 1;
		return 0;
	}
}