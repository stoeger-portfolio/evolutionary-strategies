#pragma once
#include "Helper.h"
#include <random>

class MyCommaLambda
{
public:
	MyCommaLambda(float noise, float acceptance, float minRnd, float maxRnd, uint16_t numberParents, uint16_t numberChildren);
	~MyCommaLambda();
	int Start();

private:
	std::random_device m_random_device;
	std::mt19937 m_generator;
	std::uniform_real_distribution<float> m_distribution;
	std::uniform_int_distribution<int> m_intrnd;

	float m_noise;
	float m_acceptance;
	uint16_t m_numberParents;
	uint16_t m_numberChildren;

	Helper::genom* m_bestPopulation;
	Helper::genom* m_children;
	Helper::genom* m_parents;

	void GenerateParents();
	void Mutate();
	void Select(int generation);
};

