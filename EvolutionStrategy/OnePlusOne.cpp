#include "OnePlusOne.h"
#include <assert.h>

OnePlusOne::OnePlusOne(float noise, float acceptance, float minRnd, float maxRnd) :
	m_noise(noise),
	m_acceptance(acceptance),
	m_parent(Helper::genom()),
	m_child(Helper::genom()),
	m_generator(std::mt19937_64(m_random_device())),
	m_distribution(std::uniform_real_distribution<float>(minRnd, maxRnd))
{
	printf("Constructor (1+1)\n");
}


OnePlusOne::~OnePlusOne()
{
}

int OnePlusOne::Start()
{
	float tmpa = m_distribution(m_generator);
	float tmpb = m_distribution(m_generator);

	m_parent.x = m_distribution(m_generator);
	m_parent.y = m_distribution(m_generator);

	if (tmpa > tmpb) {
		m_parent.a = tmpa;
		m_parent.b = tmpb;
	}
	else {
		m_parent.a = tmpb;
		m_parent.b = tmpa;
	}

	m_parent.result = Helper::CalculatePositiveDifference(m_parent);

	printf("(1+1) Start Algorithm\n");
	printf("New Difference: %f\n After: 0 Generations\n\n", m_parent.result);
	int generations = 1;
	while (m_parent.result > m_acceptance)
	{
		this->Mutate();
		this->Select(generations);
		generations++;
	}

	printf("Solution found after %u generations!\n\n", generations);
	printf("Accepted a difference of max: %f\n", m_acceptance);

	printf("Found x: %1.8f\n", m_parent.x);
	printf("Found y: %1.8f\n", m_parent.y);
	printf("Found a: %1.8f\n", m_parent.a);
	printf("Found b: %1.8f\n\n", m_parent.b);

	if (m_parent.a > m_parent.b)
	{
		printf("a > b = true\n\n");
	}

	printf("Left Term result: %1.8f\n", (powl(m_parent.x, 2.0f) * 3) + (powl(m_parent.y, 3.0f) * 5.0f));
	printf("Right Term result: %1.8f\n", (7.0 * m_parent.a) + (powl(m_parent.b, 2.0f) * 3.0f));
	printf("Actual difference: %f\n\n", m_parent.result);
	return generations;
}

void OnePlusOne::Mutate()
{
	float tmpa = m_distribution(m_generator);
	float tmpb = m_distribution(m_generator);

	if (tmpa > tmpb)
	{
		m_child.a = m_parent.a + m_noise * tmpa;
		m_child.b = m_parent.b + m_noise * tmpb;
	}
	else {
		m_child.a = m_parent.a + m_noise * tmpb;
		m_child.b = m_parent.b + m_noise * tmpa;
	}

	m_child.x = m_parent.x + m_noise * m_distribution(m_generator);
	m_child.y = m_parent.y + m_noise * m_distribution(m_generator);
}

void OnePlusOne::Select(int generation)
{
	m_child.result = Helper::CalculatePositiveDifference(m_child);

	if (m_child.result == INFINITY)
	{
		return;
	}

	if (m_child.result < m_parent.result) {
		// printf("New Difference: %f\nAfter: %u Generations\n\n", m_child->result, generation);
		memcpy(&m_parent, &m_child, sizeof(Helper::genom));
	}
}
