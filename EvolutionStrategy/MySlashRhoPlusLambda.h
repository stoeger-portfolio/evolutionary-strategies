#pragma once
#include "Helper.h"
#include <random>

class MySlashRhoPlusLambda
{
public:
	MySlashRhoPlusLambda(float noise, float acceptance, float minRnd, float maxRnd, uint16_t numberParents, uint16_t numberChildren, uint16_t numberDirectParents);
	~MySlashRhoPlusLambda();
	int Start();

private:
	std::random_device m_random_device;
	std::mt19937 m_generator;
	std::uniform_real_distribution<float> m_distribution;
	std::uniform_int_distribution<int> m_intrnd = std::uniform_int_distribution<int>(0, m_numberParents);

	Helper::genom* m_bestPopulation;
	Helper::genom* m_children;
	Helper::genom* m_parents;
	Helper::genom* m_population;	
	Helper::genom* m_directParents;

	float m_noise;
	float m_acceptance;

	uint16_t m_numberParents;
	uint16_t m_numberChildren;
	uint16_t m_numberDirectParents;

	void GenerateParents();
	void Mutate();
	void Select(int generation);
};

